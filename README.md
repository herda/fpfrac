**FPFRAC CONTAINS THE MATLAB CODE USED FOR NUMERICAL SIMULATIONS IN :**

--------------------------------------

*On a structure-preserving numerical method for fractional Fokker-Planck equations*

by Nathalie Ayi, Maxime Herda, Hélène Hivert, and Isabelle Tristani

Abstract: *In this paper, we introduce and analyse numerical schemes for the homogeneous and the kinetic Lévy-Fokker-Planck equation. The discretizations are designed to preserve the main features of the continuous model such as conservation of mass, heavy-tailed equilibrium and (hypo)coercivity properties. We perform a thorough analysis of the numerical scheme and show exponential stability. Along the way, we introduce new tools of discrete functional analysis, such as discrete nonlocal Poincaré and interpolation inequalities adapted to fractional diffusion. Our theoretical findings are illustrated and complemented with numerical simulations.* 

--------------------------------------

For details, please consult the paper at **HAL/Arxiv link**

--------------------------------------

**LIST OF THE SCRIPTS AND FUNCTIONS IN THIS ARCHIVE:**


* script_vmhalf.m: Script to test the convergence of the approximate vM, and to plot convergence curves. This script can be run as an example of the function vmhalf.m.

* script_homogFP.m: Script to solve the homogeneous fractional Fokker-Planck equation. Four test cases are proposed. It is possible to plot either the density of the solution, or convergence curves. 

* script_inhomogFP.m: Script to solve the kinetic Lévy-Fokker-Planck equation with the Eulerian version of the scheme. It is possible to plot either the density of the solution, convergence curves or the long time behavior of the solution. Gives Test Case 3 of the paper.

* script_semiLagFP.m: Script to solve the kinetic Lévy-Fokker-Planck equation with the Semi-Lagrangian version of the scheme, see Section 6.5 in the paper. It is possible to plot either the density of the solution, convergence curves of the long time behavior of the solution.

* evalmua.m: Function to compute the heavy-tailed equilibrium of the  Lévy-Fokker-Planck operator, see the introduction of the paper. The function uses a method of Ament and O'Neil [1], and their qastable library https://gitlab.com/s_ament/qastable.

* fraclap.m: Function to compute the discretization of the fractional Laplacian. This discretization comes from [2], it is summarized in Section 2.1 of the paper. It is adapted to densities with algebraic decay outside of the domain, see Section 3.1 of the paper.

* vmhalf.m: Function to compute an approximation of vM at interfaces. See Section 2.2 of the paper. Needs local equilibrium of the Lévy-Fokker-Planck operator computed with evalmua.m, and the matrix of the fractional Laplacian computed with fraclap.m.

* fracFP.m: Function to compute the matrix of the discrete Lévy-Fokker-Planck operator, see Sections 3 and 4 of the paper. Needs local equilibrium of the Lévy-Fokker-Planck operator computed with evalmua.m, the matrix of the fractional Laplacian computed with fraclap.m, the approximation of vM at interfaces computed with vmhalf.m, and the mass outside of the domain (Section 3.2 of the paper).

* castest.m: Function to compute the analytical solutions of Section 6.4 in the paper via symbolic calculus.

* castest_precomputed.m: Contains the result of the function castest.m. 

**ADDITIONAL DETAILS MAY BE FOUND IN THE COMMENTS OF THE XXXX.m FILES**

--------------------------------------

Contact : maxime.herda@inria.fr

--------------------------------------

**REFERENCES**

[1] Sebastian Ament and Michael O’Neil. Accurate and efficient numerical calculation of stable densities via optimized quadrature and asymptotics. *Stat. Comput.*, 28(1):171–185, 2018.

[2] Yanghong Huang and Adam Oberman. Numerical methods for the fractional Laplacian: a finite difference–quadrature approach. *SIAM J. Numer. Anal.*, 52(6):3056–3084, 2014.
