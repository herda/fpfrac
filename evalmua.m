function M = evalmua(v,a)
%EVALMUA evaluates the heavy-tailed local equilibrium defined by the
%inverse Fourier transform of xi --> exp(-|xi|^a/a) using the qastable
%library
%
% Inputs: 
%
% - v: vector of discrete velocities.
% - a: parameter of the stable density ("alpha")
%
% Output:
%
% - M: stable density evaluated at v



M = stable_sym_pdf(v*a^(1/a),a)*a^(1/a);

end

