function [ cdf ] = stable_cdf( x, a, b )
% stable cdf computes the cummulative distribution function of
% a standard stable law in Zolotarev's M parameterization
% with stability parameter a and skew parameter b

if b == 0 % symmetric case
    
    cdf = stable_sym_cdf(x, a); 
    
else % asymmetric case
    
    z = - b * tan((pi*a)/2);
    
    cdf = zeros( size(x) ) + 1/2;
    
    xlz_cond = x < z;
    if any( xlz_cond )
        
        cdf( xlz_cond ) = 1 - stable_cdf( - x( xlz_cond ), a, -b);
        
    end
    
    if any( ~xlz_cond )
        
        % number of terms to be used in the series expansions
        if 1.1 <= a

            n_inf = 80;

        elseif .9 < a || a < .5
            
            display('Error: .9 < alpha < 1.1 if beta != 0, and alpha < .5 not supported')
            cdf = NaN(size(x));
            return 
            
        elseif .5 <= a

            display('Error: asymmetric (b != 0) CDF not supported for alpha < 1.1 in this version')
            cdf = NaN(size(x));
            return
         
        end
        
        % minimum x for which the series at infinity, truncated to n terms,
        % is accurate to machine precision.
        min_inf_x = ( (1+z.^2).^(n_inf/2) ...
            .* a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));

        min_inf_x = min_inf_x + z;

        inf_cond = min_inf_x < x;
        if any( inf_cond ) 
            cdf( inf_cond ) = stable_cdf_series_infinity( ...
                x( inf_cond ), a, b, n_inf);
        end

        fourier_cond = z < x & ~inf_cond;
        if any( fourier_cond )
           cdf( fourier_cond ) = stable_cdf_integral( ...
               x( fourier_cond ) , a, b); 
        end

    end

end
    
end

