function [ bder ] = stable_pdf_bder( x, a, b )
% computes the first derivative w.r.t. a of stable densities

    
z = -b * tan(pi/2*a);

bder = zeros(size(x));

xlz_cond = x < z;
if any( xlz_cond )

    bder( xlz_cond ) = - stable_pdf_bder( -x(xlz_cond), a, -b);

end

if any( ~xlz_cond )

    if 1.1 <= a && a <= 2 

        % number of terms to be used in the series expansion
        %n_zero = 0;
        n_inf = 80;

    elseif .9 < a || a < .5
            
            display('Error: .9 < alpha < 1.1 if beta != 0, and alpha < .5 not supported')
            bder = NaN(size(x));
            return 
            
    elseif .5 <= a
        
        %n_zero = 0;
        n_inf = 90;

    end

    % minimum x for which the series at infinity, truncated to n terms,
    % converges to machine epsilon
    min_inf_x = ( (1+z.^2).^(n_inf/2) ...
            .* a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));

    min_inf_x = min_inf_x + z;

    % use finite difference approximation here
    inf_cond = min_inf_x < x;
    if any( inf_cond )
        h = 1e-5;
        
        xp1 = stable_pdf_series_infinity(...
            x(inf_cond), a, b + h, n_inf);

        xm1 = stable_pdf_series_infinity(...
            x(inf_cond), a, b - h, n_inf);

        % O(h^2) accurate finite difference
        bder( inf_cond ) = (xp1-xm1)/(2*h);
        
        %{
        h = 1e-3;
        % O(h^4) accurate finite difference
        xp2 = stable_pdf_series_infinity_OPT(...
            x(inf_cond), a, b + 2*h, n_inf);
        xm2 = stable_pdf_series_infinity_OPT(...
            x(inf_cond), a, b - 2*h, n_inf);
        bder( inf_cond)  = (-xp2 + 8*xp1 - 8*xm1 + xm2) / (12*h);
        %}
        
    end
    
    fourier_cond = ~xlz_cond & ~inf_cond;
    if any( fourier_cond )
        bder( fourier_cond ) = stable_pdf_bder_integral( ...
            x( fourier_cond ) , a, b);
    end

else
    display('Error: Input a has to be between .5 and 2');
    bder = ones(size(x)) * NaN;
end


    
% pdf = pdf / sigma;

end

