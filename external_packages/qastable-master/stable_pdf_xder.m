function [ xder ] = stable_pdf_xder( x, a, b )
% computes the first derivative w.r.t. a of stable densities

if b == 0
    
    xder = stable_sym_pdf_xder(x, a);
    
else
    
    z = -b * tan(pi/2*a);

    xder = zeros(size(x));

    xlz_cond = x < z;
    if any( xlz_cond )

        xder( xlz_cond ) = - stable_pdf_xder( -x(xlz_cond), a, -b);

    end

    if any( ~xlz_cond )

        if 1.1 <= a && a <= 2 

            % number of terms to be used in the series expansion
            n_inf = 80;
            n_zero = 0;
            
        elseif .9 < a

        elseif .5 <= a

            n_inf = 90;
            n_zero = 0;

        end
    
        % lower bound of series
        max_zero_x = 0;
        if n_zero > 0
            max_zero_x = (1+z.^2).^((n_zero+2)./(2*a*n_zero))...
                .* (eps * a .* pi * gamma(n_zero)./...
                gamma((n_zero+2)./a) ).^(1/n_zero);
        end
        
        max_zero_x = max_zero_x + z;
        
        zero_cond = ~xlz_cond & x < max_zero_x;
        if any( zero_cond )
            
            xder( zero_cond ) = stable_pdf_xder_series_zero( ...
                                x( zero_cond), a, b, n_zero);
        end
        
        % upper bound of series 
        min_inf_x = ((1+z.^2)^(n_inf/2) * a /(pi * eps) ...
            * (a*n_inf+1)*gamma(a*n_inf)/gamma(n_inf) )^(1/(a*n_inf-2));

        min_inf_x = min_inf_x + z;
        
        inf_cond = min_inf_x < x;
        if any( inf_cond )

            xder( inf_cond ) = stable_pdf_xder_series_infinity( ...
                                x( inf_cond ), a, b, n_inf);
        end

        fourier_cond = max_zero_x < x & ~inf_cond;
        if any( fourier_cond )
            xder( fourier_cond ) = stable_pdf_xder_integral( ...
                x( fourier_cond ) , a, b);
        end

    else
        display('Error: Input a has to be between .5 and 2');
        xder = ones(size(x)) * NaN;
    end

end
    
% pdf = pdf / sigma;

end

