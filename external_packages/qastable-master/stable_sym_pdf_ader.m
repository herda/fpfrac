function [ ader ] = stable_sym_pdf_ader( x, a )
% computes the first derivative w.r.t. x of symmetric stable densities

if .5 <= a && a <= 2 
   
    % number of terms to be used in the series expansion
    n_inf = 42;

    % minimum x for which the series at infinity, truncated to n terms,
    % is accurate to machine precision.
    min_inf_x = (a /(pi * eps) ...
            .* gamma(a*n_inf)./gamma(n_inf) ).^(1./(a*n_inf-1));
    
    ader = zeros(size(x));
    
    neg_inf_cond = x < -min_inf_x;
    if any(neg_inf_cond)
        x( neg_inf_cond ) = - x( neg_inf_cond );
    end
    
    % use finite difference approximation here
    inf_cond = min_inf_x < x;
    if any( inf_cond )
        h = 1e-6;
        xp1 = stable_pdf_series_infinity(...
            x(inf_cond), a + h, 0, n_inf);
        xm1 = stable_pdf_series_infinity(...
            x(inf_cond), a, 0, n_inf);
        
        ader( inf_cond ) = (xp1-xm1)/(h);
    end
    
    fourier_cond = ~inf_cond;
    if any( fourier_cond )
        ader( fourier_cond ) = stable_sym_pdf_ader_integral( ...
            x( fourier_cond ) , a);
    end
else
    display('Error: Input a has to be between .5 and 2');
    ader = NaN(size(x));
end

% pdf = pdf / sigma;
end

