function Mat = fracFP(FL, M, VMhalf,h,Ialph)
%FRACFP This function assembles the matrix of the discrete fractional 
% Fokker-Planck operator L_\alpha f = partial_v(vf)-(-\Delta)^(\alpha/2)f.
%
% Inputs: 
%
% - FL: the (2*J+1) by (2*J+1) matrix of the discrete fractional Laplacian (-\Delta)^(\alpha/2).
% - M: the (2*J+1) component vector containing the local equilibrium.
% - VMhalf: the 2*J +2 components vector of approximation of v*M at interfaces.
% - h: the step size.
% - Ialph: the approximation of (\int_L^\infty \mu_\alpha(v) dv) /
% \mu_\alpha(L) at trncation threshold L.
%
% Output:
%
% - Mat: the matrix of the discrete fractional Fokker-Planck operator



%%% Drift term d_v((VM) f/M) discretized with centered derivative

T = diag([VMhalf(2); VMhalf(3:end-1) - VMhalf(2:end-2); -VMhalf(end-1)]);
T = T + diag(VMhalf(2:end-1),1);
T = T - diag(VMhalf(2:end-1),-1);
T = T/(2*h);

T = T*diag(1./M);

%%% Boundary fluxes conserving equilibrium and mass

% On the left
T(1,:) = T(1,:) + ( h*sum(FL(2:end-1,:),1)/2  + (h+Ialph)*FL(1,:) ) / (h+Ialph);
T(1,1) = T(1,1) - Ialph*VMhalf(2)/M(1)/2/h/(Ialph+h); 
T(1,2) = T(1,2) - Ialph*VMhalf(2)/M(2)/2/h/(Ialph+h); 

% On the right
T(end,:) = T(end,:) + ( h*sum(FL(2:end-1,:),1)/2 + (h+Ialph)*FL(end,:) ) / (h+Ialph);
T(end,end) = T(end,end) + Ialph*VMhalf(end-1)/M(end)/2/h/(Ialph+h); 
T(end,end-1) = T(end,end-1) + Ialph*VMhalf(end-1)/M(end-1)/2/h/(Ialph+h); 

%%% The full discrete fractional Fokker-Planck matrix
Mat =  T - FL;

end

