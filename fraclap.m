function [Mat, beta] = fraclap(L,J,alph)
%FRACLAP this function computes the Huang and Oberman fractional Laplace
% discretization of the operator (-\Delta)^(\alpha/2). This discretization
% is adapted to densities which decay as O(|v|^{-1- \alpha}) outside of the
% domain.
%
% Inputs: 
%
% - L: the truncated domain is [-L,L].
% - J: the domain is discretized with 2*J+1 points.
% - alph: the parameter of the fractional Laplacian (-\Delta)^(\alpha/2).
%
% Outputs:
%
% - Mat: the matrix of the discrete fractional Laplacian.
% - beta: the coefficients appearing in the discrete convolution.

h = L/J; % Stepsize

%%% Integral truncation parameters
evenratio = 10;
K = evenratio*J + 1; % Must be of the form (positive even integer)*J + 1
LW = K*h; % truncation treshold of the singular integral

%%% Multiplicative constant of the fractional Laplacian:
C = alph*2^(alph-1)*gamma((alph+1)/2)/(sqrt(pi)*gamma((2-alph)/2));

%%% Computation of the Huang and Oberman coefficient corresponding to the
%%% quadrature with composite quadratic polynomials:

beta = zeros(1,K);
if alph ~= 1
    phi = @(t) t.^(2-alph)/(2-alph)/(alph-1)/alph;
    dphi = @(t) t.^(1-alph)/(alph-1)/alph;
    ddphi = @(t) -t.^(-alph)/alph;
else
    phi = @(t) t-t.*log(t);
    dphi = @(t) -log(t);
    ddphi = @(t) -1/t;
end

beta(1) = - ddphi(1) - (dphi(3)+3*dphi(1))/2 + phi(3) - phi(1) + 1/(2-alph);

k = 1:K;
kev = k(2:2:end);
kod = k(3:2:end);

beta(kev) = 2*(dphi(kev+1)+dphi(kev-1)-phi(kev+1)+phi(kev-1));

beta(kod) = -(dphi(kod+2)+6*dphi(kod)+dphi(kod-2))/2+phi(kod+2)-phi(kod-2);


%%% Last value has to be changed because of truncation of integral (K is odd)
beta(end) = -(dphi(K-2)+3*dphi(K))/2-phi(K-2)+phi(K)+ddphi(K);


beta = C * beta/h^(alph+1);
beta = [fliplr(beta),0,beta];

%%% Assembling the matrix
Mat = zeros(2*J+1,2*J+1);
for i = -J:J
    ii = i+J+1;
    Mat(ii,ii) = sum(beta)*h + 2*C/(alph*LW^alph);

    %%% Convolution in [-L,L]
    j = -J:J;
    ind = i-j;
    indd = ind+K+1;
    Mat(ii,:) = Mat(ii,:) - beta(indd)*h;

    %%% Convolution in [L,L+LW]
    j = ((J+1):(i+K));
    xj = j*h;
    ind = i-j;
    indd = ind+K+1;

    Mat(ii,end) = Mat(ii,end) - sum(beta(indd)*h.*evalmua(xj,alph)/evalmua(L,alph));

    %%% Convolution in [-L-LW,-L]
    j = ((i-K):-(J+1));
    xj = j*h;
    ind = i-j;
    indd = ind+K+1;

    Mat(ii,1) = Mat(ii,1) - sum(beta(indd)*h.*evalmua(xj,alph)/evalmua(-L,alph));
end

%%% Tails estimated with hypergeometric functions

Tail1 = hypergeom([alph+1, 2*alph+1],2*alph+2, (0:J)/K);
Tail2 = hypergeom([alph+1, 2*alph+1],2*alph+2, (0:-1:-J)/K);
Tail = (Tail1+Tail2)*C*L^(1+alph)/((2*alph+1)*LW^(2*alph+1));
Tail = [fliplr(Tail(2:end)), Tail]';
Mat(:,1) = Mat(:,1) - Tail;
Mat(:,end) = Mat(:,end) - Tail;


end

