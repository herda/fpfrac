%%% SCRIPT_HOMOGFP
%%%
%%% This script solves the fractional Fokker-Planck equation
%%%
%%% d_t f = d_v(vf)-(-Delta)^(alpha/2)f
%%%
%%% for various alpha and meshsize and plots convergence curves or solution 

close all
clear all
addpath(genpath('./external_packages'))

%%% In order to reproduce "Test case 1 : Convergence of the scheme" 
%%% in the paper choose the options Testcase = 2;tikz = 0;Drawsolution = 0;

%%% In order to reproduce "Test case 2 : Heavy tails" in the paper
%%%  choose the options Testcase = 4; tikz = 0; Drawsolution = 1;


Testcase = 4;  % 1, 2, 3 or 4. Watch out Testcase > 2 work only for density plots as no analytical formula is known for the solution.
tikz = 0;% To export tikz figures (requires matlab2tikz)

Drawsolution = 1; % 1 plots the densities; 0 plots convergence curves 

if Drawsolution
    Vecalph = 1.1; % Values of alpha
    VecJ = 512; % Number of >0 discrete velocities
else
    Vecalph = [.8, 1, 1.5]; % Values of alpha
    VecJ =  [20,40,80,160,320,640]; % Number of >0 discrete velocities
end



%%% Fundamental solution of the equation
fund = @(t,v,alph,vshift) evalmua((v - exp(-t)*vshift)/(1-exp(-t*alph))^(1/alph),alph)/(1-exp(-t*alph))^(1/alph);


L = 20; % truncated velocity domain is [-L,L]
    


figure(1)
Leg = {};
markers = {'+','o','*','.','x','s','d'};
colors = {'r','g','b','k','m','y','c'};
cpt = 1;
disp(' ')
for alph = Vecalph
    
    E = [];
    ELinf = [];
    H = [];
    
    %%% If Drawsolution = 1 the time step is fixed. Otherwise it is reduced
    %%% at each mesh refinement 
    if Drawsolution
        dt = .01;
    else
        dt = .1;
    end
    
    disp(['*** alpha = ',num2str(alph),' ***'])
    disp(' ')
    for J = VecJ
        textprogressbar(['Number of velocity points = ',num2str(2*J+1),':   '])
        
        %%% Testcase 1 is the fundamental solution, Testcase 2 is a linear
        %%% combination of shifted fundamental solutions. Testcases 3 and 4
        %%% have step functions as initial data.
        switch Testcase
            case 1
                exact_sol = @(t,v) fund(t,v,alph,0);
                massinit = 1;
            case 2
                v1 = 2;
                v2 = -6;
                exact_sol = @(t,v) .75*fund(t,v,alph,v1) + .25*fund(t,v,alph,v2);
                massinit = 1;
            case 3
                %%% Just the initial data
                exact_sol = @(t,v) ((v<=4) & (v>=0))/4.0;
                massinit = 1;
            otherwise
                %%% Just the initial data
                exact_sol = @(t,v) (1./2.)*((v<=-1) & (v>=-3)) + ((v<=4) & (v>=0))/4.0;
                massinit = 5;
        end
        
        
        
        %%% Velocity discretization
        v = linspace(-L, L, 2*J+1)';
        h = v(2)-v(1);
        
        
        %%% Initial data 
        
        if Testcase <= 2
            t = 1; % starts at t=1 because the test cases are Diracs at t=0
            tinit = t;
        else
            t = 0;
            tinit = t;
        end
        f = exact_sol(t,v);
        freal = f;
        finit = f;
        
        if Drawsolution
            T = tinit + 2; % Final time
        else
            T = tinit + .5; % total time
        end
        
        
        %%% For the mass outside of the domain
        vI = linspace(-L, L, 100000)';
        hI = vI(2)-vI(1);
        Ialph = (1-sum(evalmua(vI,alph)*hI))/2/evalmua(vI(end),alph);
        
        %%% Equilibrium
        M = evalmua(v,alph);
        finf = (sum(finit*h) + finit(end)*Ialph + finit(1)*Ialph) * M /(sum(M*h) + M(end)*Ialph + M(1)*Ialph);
        finfreal = massinit*evalmua(v,alph);
        
        
        %%% Assembling matrices
        FL = fraclap(L,J,alph);
        VM = vmhalf(FL, M, h);
        FP = fracFP(FL, M, VM,h,Ialph);
        
        % Implicit Euler
        ResMat = eye(2*J+1) - dt * FP;
        [LL,UU,pp] = lu(ResMat, 'vector');


        err = [0];
        errLinf = [0];
        Errtemps = [];
        tvec = [];
        while t<=T+dt/2
            
            %%% Plots of densities, heavy tails, long-time behavior, error
            %%% in time
            cpt2 = 0;
            if Drawsolution && t>=cpt2*T && Testcase<3
                subplot 221
                hold off
                plot(v,freal,'b-.')
                hold on 
                plot(v,f,'r')
                legend('real', 'computed')
                xlabel('v')
                ylabel('f(t,v)')
                xlim([-L,L])
                ylim([0,max(max(finit),max(finf))])
                title(['t = ',num2str(t)])
                
                subplot 222
                hold off
                loglog(v(J+2:end),freal(J+2:end),'b-.')
                hold on 
                loglog(v(J+2:end),f(J+2:end),'r')
                hold on 
                loglog(v(J+2:end),(v(J+2:end)/v(end)).^(-1-alph)*finf(end),'k--')
                legend('real', 'computed','C/v^{1+\alpha}')
                xlabel('v')
                ylabel('f(t,v)')
                xlim([v(J+2),L])
                ylim([min(min(finit(J+2:end)),min(finf(J+2:end))),max(max(finit(J+2:end)),max(finf(J+2:end)))])
                title(['t = ',num2str(t)])
                
                subplot 223
                hold on
                semilogy(t,sqrt(sum((freal-finfreal).^2./finf*h)),'b+')
                hold on 
                semilogy(t,sqrt(sum((f-finf).^2./finf*h)),'ro')
                legend('real', 'computed')
                xlabel('t')
                ylabel('||f(t,v) - C\mu_\alpha(v)|| (weighted L^2 norm)')
                xlim([tinit,T])
                %ylim([1e-8,10*sqrt(sum((finit-finf).^2./finf*h))])
                set(gca, 'YScale', 'log')
                
                subplot 224
                hold on
                semilogy(t,err(end),'g+')
                hold on 
                semilogy(t,errLinf(end),'mo')
                legend('weighted L2', 'Linf')
                xlabel('t')
                ylabel('error with analytic solution')
                xlim([tinit,T])
                %ylim([1e-8,10*sqrt(sum((finit-finf).^2./finf*h))])
                set(gca, 'YScale', 'log')
                
                drawnow
                
                cpt2 = cpt2 + 1/10;
            end
            
            if Drawsolution && t>=cpt2*T && Testcase>2
                subplot 221
                hold off
                plot(v,freal,'b-.')
                hold on 
                plot(v,f,'r')
                legend('Initial data', 'computed')
                xlabel('v')
                ylabel('f(t,v)')
                xlim([-L,L])
                ylim([0,max(max(finit),max(finf))])
                title(['t = ',num2str(t)])
                
                subplot 222
                hold off
                loglog(v(J+2:end),freal(J+2:end),'b-.')
                hold on 
                loglog(v(J+2:end),f(J+2:end),'r')
                hold on 
                loglog(v(J+2:end),(v(J+2:end)/v(end)).^(-1-alph)*finf(end),'k--')
                legend('Initial data', 'computed','C/v^{1+\alpha}')
                xlabel('v')
                ylabel('f(t,v)')
                xlim([v(J+2),L])
                ylim([min(min(finit(J+2:end)),min(finf(J+2:end))),max(max(finit(J+2:end)),max(finf(J+2:end)))])
                title(['t = ',num2str(t)])
                
                
                
                subplot 223
                tvec = [tvec, t];
                Errtemps = [Errtemps,sqrt(sum((f-finf).^2./finf*h))];
                hold on
                semilogy(t,Errtemps(end),'b+')
                legend('computed')
                xlabel('t')
                ylabel('||f(t,v) - C\mu_\alpha(v)|| (weighted L^2 norm)')
                xlim([tinit,T])
                %ylim([1e-8,10*sqrt(sum((finit-finf).^2./finf*h))])
                set(gca, 'YScale', 'log')
                
                drawnow
                
                cpt2 = cpt2 + 1/10;
            end
            
            %%% Resolution
            
            
            % Implicit Euler with precomputed LU decomposition
            f = f(pp);
            opts.UT = false;
            opts.LT = true;
            f = linsolve(LL, f, opts);
            opts.LT = false;
            opts.UT = true;
            f = linsolve(UU, f, opts);
            
            
            %%% Compararison with exact solution 
            freal = exact_sol(t,v);
            err = [err, sqrt(sum(abs(f-freal).^2./finf*h))/sqrt(sum(abs(finit).^2./finf*h))];
            errLinf = [errLinf, max(abs(f-freal))/max(abs(finit))];
            
            
            textprogressbar((t-tinit)/(T-tinit)*100)
            t = t+dt;
        end
        textprogressbar(100)
        textprogressbar('done')
        if ~Drawsolution
            % Implicit Euler
            dt = dt/4; % For convergence tests to capture up to 2nd order in velocity
        end
        
        
        %%% Distance to equilibrium plot (longtime)
        if Drawsolution && Testcase > 2
            figure(2)
            semilogy(tvec,Errtemps,'b+')
            legend('computed')
            xlabel('t')
            ylabel('||f(t,v) - C\mu_\alpha(v)|| (weighted L^2 norm)')
            xlim([tinit,T])
            set(gca, 'YScale', 'log')
            if tikz
                matlab2tikz('longtime.tex')
            end
        end
        
        H = [H,h];
        E = [E, max(err)];
        ELinf = [ELinf, max(errLinf)];
    end
    
    %%% Convergence plots
    if ~Drawsolution && Testcase<=2
        subplot 121
        loglog(H, E, ['-',markers{cpt},colors{cpt}])
        xlabel('Meshsize')
        ylabel('Error in weighted L^2 norm')
        hold on
        
        subplot 122
        loglog(H, ELinf, ['-',markers{cpt},colors{cpt}])
        xlabel('Meshsize')
        ylabel('Error in weighted L^{\infty} norm')
        hold on
        Leg = [Leg,{['\alpha = ',num2str(alph)]}];
        disp(['Approximate order is ', num2str((log(E(end))-log(E(end-1)))/(log(H(end))-log(H(end-1))))])
    end
    cpt = cpt + 1;
    disp(' ')
end



if ~Drawsolution
    legend(Leg,'Location','Southeast')
end

if tikz && ~Drawsolution
    matlab2tikz('cvplots.tex')
end

if tikz && Drawsolution
    figure(1)
    matlab2tikz('densities.tex')
end
