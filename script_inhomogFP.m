%%% SCRIPT_INHOMOGFP
%%%
%%% This script solves the kinetic Lévy-Fokker-Planck equation 
%%%
%%% d_t f + vd_xf = d_v(vf)-(-Delta)^(alpha/2)f
%%%
%%% with centered finite difference/volume for transport and the
%%% structure preserving scheme in velocity


close all
clear all
addpath(genpath('./external_packages'))



Drawsolution = 1; % 1 plots the densities; 0 plots convergence curves 
DrawLongtime = 1;
tikz = 0; % writes results in tikz file (requires matlab2tikz package)

%Test Case 3 of the paper is obtained with Drawsolution = 1 and DrawLongtime= 1

%% Analytical solution

Precomputed = 1; % To avoid running symbolic computation at each simulation (must be set to 0 if 'testcase_precomputed.m' does not exist, requires symbolic computation Toolbox)


if ~Precomputed
    disp('Computing the analytic solution with symbolic calculus...')
    ct = testcase();
    disp('--------')
else
    ct = testcase_precomputed();
end 


alph = 1;

tau = @(t) 1-exp(-t);
eta = @(t) t-tau(t);
w = @(v,v0,t) v-v0*exp(-t);
y = @(x,v0,t) x-v0*tau(t);



t0 = .5;
v0 = 1;
realsol = @(t,x,v) ct(eta(t+t0),tau(t+t0),w(v,v0,t+t0),y(x,v0,t+t0));


%% Discretization

xmax = 2*pi;
L = 16; % truncation of the velocity domain

if Drawsolution
    refineVec = 4; %Refinement number 
else
    refineVec = 2:4; %Refinement number
end
refineVec = round(exp(refineVec*log(2)));% 2^k with k in refineVec
J0 = 2;
Nx0 = 8;
VecNx = Nx0*refineVec;
VecJ = J0*refineVec;
errVec = [];
timeVec = [];
deltVec = [];

disp('Solving kinetic Fokker-Planck equation')
disp('--------')


%%% Loop on meshsize
for cpt = 1:length(VecJ)
    
    
    %%% Mesh
    J = VecJ(cpt);    
    Nx = VecNx(cpt); 
    Nv = 2*J+1;
    dx = xmax/Nx;
    x = (0:(Nx-1))*dx;
    v = linspace(-L,L,Nv)';
    h = v(2)-v(1);
    dt = 1e-2;
    

    disp(['Time step = ', num2str(dt)])
    disp(['Nx = ',  num2str(Nx)])
    disp(['Nv = ',  num2str(Nv)])
    
    
    %%% Initial data 

    init = @(x,v) realsol(0,x,v);
    [X,V] = meshgrid(x,v);
    F = init(X,V);
    Freal = F;
    f = reshape(F', Nx*Nv,1);
    freal = f;
    finit = f;
    massinitreal = xmax;


    %%% For the mass outside of the domain
    vI = linspace(-L, L, 100000)';
    hI = vI(2)-vI(1);
    Ialph = (1-sum(evalmua(vI,alph)*hI))/2/evalmua(vI(end),alph);

    
    %%% Equilibrium
    M = evalmua(v,alph);
    finf = M /(sum(M*h) + M(end)*Ialph + M(1)*Ialph);
    finfreal = evalmua(v,alph);
    massinit = sum(f*h*dx) + Ialph*(sum(F(end,:)) + sum(F(1,:)))*dx;
    globfinf = massinit * repelem(finf',Nx)'/xmax;
    globfinfreal = massinitreal * repelem(finfreal',Nx)'/xmax;
    
    %%% Macroscopic density
    rho = sum(F,1)*h + Ialph*((F(end,:)) + (F(1,:)));
    rhoinit = rho;
    rhoreal = sum(Freal,1)*h + Ialph*((Freal(end,:)) + (Freal(1,:)));
    
    %%% Assembling matrices
    textprogressbar('Assembling matrices: ')
    
    % Fractional Fokker-Planck matrix
    FL = fraclap(L,J,alph);
    VM = vmhalf(FL, M, h);
    FP = fracFP(FL, M, VM,h,Ialph);

    % Building resolution matrix
    ResMat = sparse(Nx*Nv, Nx*Nv);   
    indx = @(i) mod((i-1),Nx) + 1; % Periodic index in x
    ind = @(i,j) round(indx(i) + (j-1)*Nx);% Index in x,v (along x and then along v)
    CFLx = dt/dx;
    for i = 1:Nx
        for  j = 1:Nv
            ResMat(ind(i,j), ind(i+1,j)) = CFLx * v(j) / 2;
            ResMat(ind(i,j), ind(i-1,j)) = - CFLx * v(j) / 2; 
            for jj = 1:Nv
                ResMat(ind(i,j), ind(i,jj)) = -dt*FP(j,jj);
            end
        end
        textprogressbar(i*100/Nx)
    end
    textprogressbar(100)
    textprogressbar('done')
    ResMat = ResMat + sparse(eye(Nx*Nv));
    
    

    %%% Time parameters
    T = 35;
    t = [0];
    
    %%% Error of approximation
    err = 0;
    
    %%% Distance to equilibrium
    timeerr = [sqrt(sum((f-globfinf).*(f-globfinf)./globfinf*h*dx))];
    timeerrreal = [sqrt(sum((freal-globfinfreal).*(freal-globfinfreal)./globfinfreal*h*dx))];
    
    
    %%% Time loop
    tic
    textprogressbar('Computing   ')
    while t(end)<T-dt/10
        
        % Density plots
        if Drawsolution
            subplot 211
            hold off
            surf([X, xmax*ones(size(X,1),1)],[V, V(:,1)],[F, F(:,1)])
            xlabel('x')
            xlim([0,xmax])
            ylabel('v')
            title(['Microscopic distribution f(t,x,v) at t = ', num2str(t(end))])
            colorbar
            shading interp
            view(2)
            subplot 212
            hold off
            plot([x, xmax],[rho, rho(1)])
            xlabel('x')
            xlim([0,xmax])
            ylabel('\rho')
            title(['Macroscopic density \rho(t,x) at t = ', num2str(t(end))])
            ylim([min(rhoinit), max(rhoinit)])
            drawnow
        end
        
        %%% Solving the scheme and computing densities
        t = [t,t(end)+dt];
        f = ResMat\f;
        F = reshape(f, Nx,Nv)';
        rho = sum(F,1)*h + Ialph*((F(end,:)) + (F(1,:)));
        Freal = realsol(t(end),X,V);
        freal = reshape(Freal', Nx*Nv,1);
        
        %%% Error of approximation
        err = max(err, max(max(abs(F-Freal))));
        
        %%%% Distance to equilibrium
        timeerr = [timeerr, sqrt(sum((f-globfinf).*(f-globfinf)./globfinf*h*dx))];
        timeerrreal = [timeerrreal, sqrt(sum((freal-globfinfreal).*(freal-globfinfreal)./globfinfreal*h*dx))];
        
        textprogressbar(t(end)/T*100)
    end
    textprogressbar('done')
    errVec = [errVec, err];
    timeVec = [timeVec, toc];
    disp('--------')
    
    %%% Plot of longtime behavior
    if DrawLongtime
        figure
        semilogy(t,timeerr, 'k')
        hold on
        semilogy(t,timeerrreal, 'r-.')
        xlabel('t')
        ylabel('Distance to equilibrium')
        legend('Approximate', 'Real')
        drawnow
        if tikz
            matlab2tikz('longtime_inhom.tex')
        end
    end
end

%%% Plot of convergence curves
if ~Drawsolution 
    figure
    loglog(2*L./(2*VecJ+1), errVec, '-o')
    xlabel('Meshsize')
    ylabel('Error in L^{\infty} norm')
end
