%%% SCRIPT_vmhalf
%%%
%%% This script tests the convergence of the approximate (VM) 
%%% computed thanks to the discrete fractional Laplacian of M


close all
clear all
addpath(genpath('./external_packages'))

figure
Leg = {};
markers = {'+','o','*','.','x','s','d'};
colors = {'r','g','b','k','m','y','c'};

alphVec = [.6,.8,1,1.2,1.4,1.6];
JVec = [4,8,16,32,64,128,256];

cpt = 1;

%%% Loop on alpha
for alph = alphVec
    Einf = [];% Error in absolute norm
    H = [];% Stepsize
    
    cpt2 = 1;
    textprogressbar(['Computing for alpha = ',num2str(alph),':   '])
    
    %%% Loop on meshsize
    for J = JVec    

        L = 16; % truncation of domain
        v = linspace(-L, L, 2*J+1)'; % cell centers
        h = v(2)-v(1); % stepsize
        vhalf = [-L-h/2, v'+h/2]; % cell interfaces
        H = [H,h];

        %%% Computing equilibrium
        M = evalmua(v,alph);
        Mhalf = evalmua(vhalf,alph);

        %%% Computing matrices
        FL = fraclap(L,J,alph);

        %%% Computing VM
        VM = vmhalf(FL, M, h)';
        VMreal = Mhalf.*vhalf; 


        %%% Computing the error
        errLinf = max(abs(VM-VMreal));
        Einf = [Einf, errLinf];
        textprogressbar(cpt2/length(JVec)*100)
        cpt2 = cpt2+1;
    end
    
    %%% Convergence plots
    textprogressbar(100)
    textprogressbar('done')
    loglog(H, Einf, ['-',markers{cpt},colors{cpt}])
    hold on
    Leg = [Leg,{['\alpha = ',num2str(alph)]}];
    cpt = cpt + 1;
end

legend(Leg,'Location','Southeast')
xlabel('Number of velocity points')
ylabel('Error of approximation in L^{\infty} norm')
title('Convergence of the approximate vM')


