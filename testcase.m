function ct = castest()
% CASTEST computes via symbolic calculus the integrals appearing in the 
% explicit solution of  d_t f + vd_xf = d_v(vf)-(-\Delta_v)^(1/2)f proposed
% in the paper. 
%
% Output:
% - ct is the function handle containing the result. It is a function of
% eta, tau, w and y (see the paper for details). A *.m function file is 
% created with the result in order to avoid to run the symbolic computation
% at each simulation.

syms xi w y real
syms tau eta positive

I1 = simplify(int(exp(-tau*xi-eta)*cos(xi*w+y),xi,0,inf));

I2co = simplify(int((1-xi)^2*exp((2-tau)*xi-eta)*cos(xi*w),xi,-tau/(1-tau),0));
I2si = simplify(int((1-xi)^2*exp((2-tau)*xi-eta)*sin(xi*w),xi,-tau/(1-tau),0));

I3 = simplify(int(exp(tau*xi+eta)*cos(xi*w+y),xi,-inf,-tau/(1-tau)));

I = I1 + I2co*cos(y)-I2si*sin(y)+I3;
I = (I + tau/(tau^2+w^2))/pi;
ct = matlabFunction(I);
fil = fopen('castest_precomputed.m','w');
fprintf(fil, '%s', 'function res = castest_precomputed()');
fprintf(fil, '\n');
fprintf(fil, '%s', ['res = ', func2str(ct),';']);
fprintf(fil, '\n');
fprintf(fil, '%s', 'end');
fclose(fil);

end




