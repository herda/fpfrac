function VM = VMhalf(FL, M, h)
%VMHALF computes an approximation of v*M at interfaces based on the
%identity v*M = (\int_{-v}^v (-\Delta_v)^{\alpha/2}M(w) dw) / 2
%
% Input:
% - FL is the (2*J+1) by (2*J+1) matrix of the fractional Laplacian
% - M is the (2*J+1) component vector of local equilibrium at cell centers
% - h is the stepsize
%
% Output:
% - VM is the (2*J+2) component vector approximating v*M at cell interfaces

J = (length(M)-1)/2;
ind = -J:J;
Integ_Mat = abs(ind)<=ind';
Integ_Mat = Integ_Mat(J+1:end,:);
Integ_Mat = [-Integ_Mat((J+1):-1:1,:);Integ_Mat];
VM = Integ_Mat*FL*M*h/2;


end

